

<!DOCTYPE HTML>
<!--
	Snapshot by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Fitmap</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <style>
        body {font-family: Verdana;}

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #19B5FE;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;

        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
</head>
<body>
<div class="page-wrap">

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="index.php"><span class="icon fa-home"</span></a></li>
            <li><a href="profilo.php"><span class="icon fa-user"></span></a></li>
            <li><a href="amministrazione.php" class="active"><span class="icon fa-calendar"></span></a></li>
            <li><a href="logout.php"><span class="icon fa-sign-out"></span></a></li>
        </ul>
    </nav>

    <!-- Main -->
    <section id="main">

        <!-- Banner -->
        <!--<section id="banner">
            <div class="inner">
                <h1>FitMap</h1>
                <p>La palestra DOVE vuoi, QUANDO vuoi</p>
            </div>
        </section>



        <!-- Gallery -->
        <section id="galleries">

            <!-- Photo Galleries -->
            <div class="gallery">
                <header class="special">
                    <h2 style="margin-top: 20px">SETTIMANA</h2><br>
                </header>

                <!-- Filters -->
                <header>
                    <h1 style="margin-left: 20px">Giorno</h1>
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'lunedi')">Lunedì</button>
                        <button class="tablinks" onclick="openCity(event, 'martedi')">Martedì</button>
                        <button class="tablinks" onclick="openCity(event, 'mercoledi')">Mercoledì</button>
                        <button class="tablinks" onclick="openCity(event, 'giovedi')">Giovedì</button>
                        <button class="tablinks" onclick="openCity(event, 'venerdi')">Venerdì</button>
                        <button class="tablinks" onclick="openCity(event, 'sabato')">Sabato</button>
                        <button class="tablinks" onclick="openCity(event, 'domenica')">Domenica</button>
                    </div>
                </header>

                <div id="lunedi" class="tabcontent">
                    <h3>Lunedì</h3>
                    <p>London is the capital city of England.</p>
                </div>

                <div id="martedi" class="tabcontent">
                    <h3>Martedì</h3>
                    <p>Paris is the capital of France.</p>
                </div>

                <div id="mercoledi" class="tabcontent">
                    <h3>Mercoledì</h3>
                    <p>Tokyo is the capital of Japan.</p>
                </div>

                <div id="giovedi" class="tabcontent">
                    <h3>Giovedì</h3>
                    <p>Tokyo is the capital of Japan.</p>
                </div>

                <div id="venerdi" class="tabcontent">
                    <h3>Venerdì</h3>
                    <p>Tokyo is the capital of Japan.</p>
                </div>

                <div id="sabato" class="tabcontent">
                    <h3>Sabato</h3>
                    <p>Tokyo is the capital of Japan.</p>
                </div>

                <div id="domenica" class="tabcontent">
                    <h3>Domenica</h3>
                    <p>Tokyo is the capital of Japan.</p>
                </div>

                <script>
                    function openCity(evt, cityName) {
                        var i, tabcontent, tablinks;
                        tabcontent = document.getElementsByClassName("tabcontent");
                        for (i = 0; i < tabcontent.length; i++) {
                            tabcontent[i].style.display = "none";
                        }
                        tablinks = document.getElementsByClassName("tablinks");
                        for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                        }
                        document.getElementById(cityName).style.display = "block";
                        evt.currentTarget.className += " active";
                    }
                </script>
            </div>
        </section>

        <!-- Contact -->
        <section id="contact">
            <!-- Form -->
            <div class="column">
                <h3>Inserisci Lezione</h3>
                <form action="conf/corso.php" method="post">
                    <div class="field half first">
                        <select name="giorno" id="giorno">
                            <option value="Lunedì">Lunedì</option>
                            <option value="Lunedì">Martedì</option>
                            <option value="Lunedì">Mercoledì</option>
                            <option value="Lunedì">Giovedì</option>
                            <option value="Lunedì">Venerdì</option>
                            <option value="Lunedì">Sabato</option>
                            <option value="Lunedì">Domenica</option>
                        </select>
                    </div>
                    <div class="field half">
                        <label for="orario">Orario</label>
                        <input name="orario" id="orario" type="time">
                    </div>
                    <div class="field">
                        <label for="lezione">Lezione</label>
                        <input name="lezione" type="text" id="lezione">
                    </div>
                    <ul class="actions">
                        <li><input value="Invia" class="button" type="submit"></li>
                    </ul>
                </form>
            </div>


            <!-- Social -->
            <div class="social column">
                <h3>About Me</h3>
                <p>Mus sed interdum nunc dictum rutrum scelerisque erat a parturient condimentum potenti dapibus vestibulum condimentum per tristique porta. Torquent a ut consectetur a vel ullamcorper a commodo a mattis ipsum class quam sed eros vestibulum quisque a eu nulla scelerisque a elementum vestibulum.</p>
                <p>Aliquet dolor ultricies sem rhoncus dolor ullamcorper pharetra dis condimentum ullamcorper rutrum vehicula id nisi vel aptent orci litora hendrerit penatibus erat ad sit. In a semper velit eleifend a viverra adipiscing a phasellus urna praesent parturient integer ultrices montes parturient suscipit posuere quis aenean. Parturient euismod ultricies commodo arcu elementum suspendisse id dictumst at ut vestibulum conubia quisque a himenaeos dictum proin dis purus integer mollis parturient eros scelerisque dis libero parturient magnis.</p>
                <h3>Follow Me</h3>
                <ul class="icons">
                    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                </ul>
            </div>

        </section>

        <!-- Footer -->
        <footer id="footer">
            <div class="copyright">
                &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images: <a href="https://unsplash.com/">Unsplash</a>.
            </div>
        </footer>
    </section>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>