<!DOCTYPE HTML>
<!--
	Snapshot by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Fitmap</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>
<body>
<div class="page-wrap">

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="index.php"><span class="icon fa-home"</span></a></li>
            <li><a href="profilo.php" class="active"><span class="icon fa-user"></span></a></li>
            <li><a href="amministrazione.php"><span class="icon fa-calendar"></span></a></li>
            <li><a href="logout.php"><span class="icon fa-sign-out"></span></a></li>
        </ul>
    </nav>

    <!-- Main -->
    <section id="main">

        <!-- Banner -->
        <!--<section id="banner">
            <div class="inner">
                <h1>FitMap</h1>
                <p>La palestra DOVE vuoi, QUANDO vuoi</p>
            </div>
        </section>



        <!-- Gallery -->
        <section id="galleries">

            <!-- Photo Galleries -->
            <div class="gallery">
                <header class="special">
                    <h2 style="margin-top: 20px">MODIFICA PROFILO PALESTRA</h2><br>
                </header>

                <div class="content" style="margin-top: 20px; margin-left: 20px" >
                        <div class="text-center">
                            <img src="http://lorempixel.com/200/200/people/9/" class="avatar img-circle img-thumbnail" alt="avatar">
                            <h6>Cambia immagine del profilo</h6>
                            <input type="file" class="text-center center-block well well-sm">
                        </div>
                    </div>
                </div>
        </section>

        <!-- Contact -->
        <section id="contact">
            <!-- Form -->
            <div class="column">
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                    <h3>Informazioni Personali</h3>
                    <form class="form-horizontal" role="form" method="post" action="conf/profile.php">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Nome Palestra:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="nome_palestra" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Nome:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="nome" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Cognome:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="cognome" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Email:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="email" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Città:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="citta" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Indirizzo:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="indirizzo" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Username:</label>
                            <div class="col-md-8">
                                <input class="form-control" name="username" value="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password:</label>
                            <div class="col-md-8">
                                <input class="form-control" name="pass" value="" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input class="btn btn-primary" value="Salva e Invia" type="button">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </section>

        <!-- Footer -->
        <footer id="footer">
            <div class="copyright">
                &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images: <a href="https://unsplash.com/">Unsplash</a>.
            </div>
        </footer>
    </section>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>