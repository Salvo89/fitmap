<?php
session_start(); //inizio la sessione

require "conf/db.php";

$db= new MyDb();

$conn= $db->getConnection();

$id = $_SESSION['cod'];

$query = "SELECT
    `id`,
    `nome_palestra`,
    `nome`,
    `cognome`,
    `email`,
    `citta`,
    `indirizzo`
FROM
    `palestra`
WHERE
    `id` = '$id'";
$ris = mysqli_query($conn, "$query") or die ("Errore nella query <br> $query");
$riga=mysqli_fetch_assoc($ris);

$nome_palestra = $riga['nome_palestra'];
$nome = $riga['nome'];
$cognome = $riga['cognome'];
$email = $riga['email'];
$citta = $riga['citta'];
$indirizzo = $riga['indirizzo'];

?>

<!DOCTYPE HTML>
<!--
	Snapshot by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Fitmap</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>
<body>
<div class="page-wrap">

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="index.php"><span class="icon fa-home"</span></a></li>
            <li><a href="profilo.php" class="active"><span class="icon fa-user"></span></a></li>
            <li><a href="amministrazione.php"><span class="icon fa-calendar"></span></a></li>
            <li><a href="logout.php"><span class="icon fa-sign-out"></span></a></li>
        </ul>
    </nav>

    <!-- Main -->
    <section id="main">

        <!-- Banner -->
        <!--<section id="banner">
            <div class="inner">
                <h1>FitMap</h1>
                <p>La palestra DOVE vuoi, QUANDO vuoi</p>
            </div>
        </section>



        <!-- Gallery -->
        <section id="galleries">

            <!-- Photo Galleries -->
            <div class="gallery">
                <header class="special">
                    <h2 style="margin-top: 20px">PROFILO PALESTRA</h2><br>
                </header>

                <div class="content" style="margin-top: 20px; margin-left: 20px" >
                    <div class="text-center">
                        <img src="http://lorempixel.com/200/200/people/9/" class="avatar img-circle img-thumbnail" alt="avatar">
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact -->
        <section id="contact">
            <!-- Form -->
            <div class="column">
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                    <h3>Informazioni Personali</h3><br>
                    <h2>NOME PALESTRA: <?php echo "$nome_palestra"?></h2>
                    <h2>NOME: <?php echo "$nome"?></h2>
                    <h2>COGNOME: <?php echo "$cognome"?></h2>
                    <h2>EMAIL: <?php echo "$email"?></h2>
                    <h2>CITTA': <?php echo "$citta"?></h2>
                    <h2>INDIRIZZO: <?php echo "$indirizzo"?></h2></div>
            </div>

        </section>

        <!-- Footer -->
        <footer id="footer">
            <div class="copyright">
                &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images: <a href="https://unsplash.com/">Unsplash</a>.
            </div>
        </footer>
    </section>
</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/jquery.scrolly.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>